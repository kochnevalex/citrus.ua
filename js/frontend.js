$(document).ready(function () {
    function goodTab() {
        var $thumbnail = $('.goods-specification__thumbnail li a');
        $thumbnail.on('click', function (e) {
            e.preventDefault();
            var dataColor = $(this).attr('data-bg'),
                dataItem = 'img[data-color=' + $(this).attr('data-color') + ']';
            $thumbnail.removeClass('active');
            $(this).addClass('active');
            $(this).closest('.goods-specification__wrap').find('img').removeClass('active');
            $(this).closest('.goods-specification__wrap').find(dataItem).addClass('active');
            $(this).closest('.goods-specification__wrap').find('.goods-specification__description-bg path').css('fill', dataColor);
            $(this).closest('.goods-specification__wrap').find('h2').css('color', dataColor);
        })
    }

    function toggleVariables() {
        var $btn = $('.show-btn');
        $btn.on('click', function (e) {
            e.preventDefault();
            $(this).closest('.show-wrap').find('.show-main').toggleClass("active");
            $(this).closest('.show-wrap').find('.show-extra').toggleClass('active');
            $(this).toggleClass('info');
            $(this).hasClass('info') ? $(this).find('span').text($(this).attr('data-info')) : $(this).find('span').text($(this).attr('data-performance'));
        });
    }

    function stickyNav() {
        var $headerNav = $('#headerNavBottom'),
            $headerNavWrap = $('.header__nav--bottom-wrap'),
            headerNavWrapHeight = $('.header__nav--bottom-wrap').height(),
            headerNavTop = $headerNav.offset().top;
        $(window).scroll(function () {
            if ($(window).scrollTop() >= headerNavTop) {
                $headerNavWrap.css('height', headerNavWrapHeight);
                $headerNav.addClass('sticky');
            } else {
                $headerNavWrap.css('height', 'auto');
                $headerNav.removeClass('sticky');
            }
        });
    }

    function scrollTo() {
        var $btn = $('.scroll-btn[href*=\\#]:not([href=\\#])'),
            headerNavWrapHeight = $('.header__nav--bottom-wrap').height();
        $btn.on('click', function (e) {
            e.preventDefault();
            var position = $(this).attr('href');
            if ($(position).length) {
                position = parseFloat($(position).offset().top) - parseFloat(headerNavWrapHeight);
                $("html, body").animate({scrollTop: position}, 600);
            }


        });
    }

    function activeMenu() {
        var $menuItem = $('.header__nav--bottom li a:not(.btn)');
        $menuItem.on('click', function () {
            $(this).closest('li').siblings().find('a').removeClass('active');
            $(this).addClass('active');
        });
        var $scrollItem = $('.header__nav--bottom ul li:not(.header__nav--bottom-logo) .scroll-btn:not(.btn)');
        $(window).scroll(function () {
            $scrollItem.each(function () {

                if (($($(this).attr('href')).offset().top - $(window).height() / 2 <= $(window).scrollTop()) && (($($(this).attr('href')).offset().top + $($(this).attr('href')).height() - $(window).height() / 2 - 50) >= $(window).scrollTop() )) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });

        });
    }

    function popup() {
        var $btn = $('.popup-btn');
        $btn.magnificPopup({
            type: 'inline',
            callbacks: {
                close: function () {
                    $(this.ev).removeClass('active');

                }
            }
        })
    }

    goodTab();
    toggleVariables();
    stickyNav();
    scrollTo();
    activeMenu();
    popup();
    AOS.init();
});
